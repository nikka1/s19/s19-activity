const getCube = 2;
console.log(`The cube of ${getCube} is ${getCube ** 3}`);

const fullAddress = ["258", "Washington Ave", "NW", "California", "90011"];

const [bldgNo, streetName, city, state, zipCode] = fullAddress;
console.log(`I live at ${bldgNo} ${streetName} ${city}, ${state} ${zipCode}.`)

const animal = {
	animalName: "Lolong",
	animalType: "saltwater crocodile",
	weight: "1075 kg",
	length: "20 ft 3 in"
}

const {animalName, animalType, weight, length} = animal;
function getAnimalDesc({animalName, animalType, weight, length}) {
	console.log(`${animalName} was a ${animalType}. He weighed at ${weight} with a measurement of ${length}`)
}

getAnimalDesc(animal)


const numList = [1, 2 ,3, 4,5]
numList.forEach( (integer) => {
	console.log(integer)
})

class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog("Chandler", 1, "Corgi")
console.log(myDog)